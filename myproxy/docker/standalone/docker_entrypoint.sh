#!/bin/bash

set -e

if [[ -z "$1" ]]; then

  echo "MyProxy Gateway host cert:"
  export TRUSTED_HOSTCERT=$(cat /myproxy-gateway/hostcerts/mpgcert.pem)
  echo "$TRUSTED_HOSTCERT"
  echo ""
  echo "MyProxy Gateway host key:"
  export TRUSTED_HOSTKEY=$(cat /myproxy-gateway/hostcerts/new-mpgkey.pem)
  echo "$TRUSTED_HOSTKEY"
  echo ""

fi

container_ip=$(grep $HOSTNAME /etc/hosts | awk '{print $1}')
if [[ -z "$'mpg.base.url'" ]]; then
  'mpg.base.url'="https://$container_ip"
fi

if [[ -z "$(grep "docker.example.com" /etc/hosts)" ]]; then
  echo "${container_ip} docker.example.com" >> /etc/hosts
fi

#find /myproxy-gateway/mpg-docs/target/classes
#sed -ri "s/https:\/\/docker.example.com/https:\/\/$mpg.base.url/g" /var/www/html/myproxy/v1/docs/resources/index.html
#sed -ri "s/https:\/\/docker.example.com/https:\/\/$mpg.base.url/g" /var/www/html/myproxy/v1/docs/resources/myproxy

exec "$@"
