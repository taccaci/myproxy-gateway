# The MyProxy Gateway ================================
This folder contains a passwordless host cert and key that can be used by the MPG to authenticate to the trusted MyProxy CA in the included Docker image. 


## Generating new host keys

If you choose to build and use the included Dockerfile to run your development MyProxy server, you can use the following commands to export a fresh set of service credentials you can use. Remember that, by default, the Dockerfile will assume a hostname of docker.example.com. You can edit this in the Dockerfile and the examples below to a different hostname with a simple file and replace. Rebuilding the container will generate fresh credentials for you. 

By default, the host cert and key of the MPG will be printed to stdout on startup. To manually grab the keys without starting up the container processes, you can run the following.

  $ docker run -h docker.example.com -i agave-test-mpg cat /myproxy-gateway/hostcerts/mpgcert.pem > ~/.globus/hostcert.pem
  $ docker run -h docker.example.com -i agave-test-mpg cat /myproxy-gateway/hostcerts/new-mpgkey.pem > ~/.globus/hostkey.pem
  $ chmod 600 ~/.globus/hostkey.pem

To generate a completely new set of keys, run the following and paste the contents into the appropriate files.

  $ docker run -h docker.example.com -i \
        agave-test-mpg \
        grid-cert-request -host somehost.example.com -dir . -service somehost -prefix somehost -nopw && \
        echo "globus" | grid-ca-sign -days 3650 -in somehostcert_request.pem -out somehostcert.pem && \
        openssl rsa -in somehostkey.pem -outform PEM -out new-somehostkey.pem && \
        echo "New host cert is" && cat somehostcert.pem && echo "New host key is: " && cat new-somehostkey.pem

**NOTE** If you rebuild the container, the MyProxy CA cert will change and your keys will be invalidated. You will need to regenerate them afterwards.
