# MyProxy Server Setup
******

> Thanks to Terry Fleury for his input and guidance sorting through the following MyProxy setup based on the one used by the [CILogon](http://www.cilogon.org/) project.


This document covers the needed configuration and setup of a MyProxy server to use in combination with the OA4MP service running as protected resource behind an OAuth 2 server. If you are running OA4MP as a standalone RESTful proxy to an existing MyProxy service, you most likely do not need to change anything. OA4MP will work out of the box. 

## Installing MyProxy

If you are running a MyProxy server already, you can leave that in place to serve your existing user base. For use with OA4MP, we recommend running a second instance on another port as you will not be able to create the trust relationship necessary for OA4MP and still allow other users to retrieve credentials.

Installing MyProxy is generally a straightforward process. If you have not done so already, follow the direction on the [MyProxy website](http://grid.ncsa.illinois.edu/myproxy/ca/) to install the server and configure it as a Certificate Authority (CA).

Next, the MyProxy server should be locked down (e.g. via iptables) so 
that only the OA4MP server can talk to the MyProxy server. This will reduce the possiblity that the server can be compromised by attackers or taken down by things such as denial of service attacks.

## Obtaining a service credential

In order for the MyProxy server to trust the OA4MP service, OA4MP must present a valid credential to the MyProxy server on every request. We can create a certificate request for the OA4MP service and use the MyProxy server's CA to sign it. As the user running the service, create a cert request. In this example, we create a passwordless key. You may choose to do otherwise depending on your needs.

```
#!bash
$ grid-cert-request -host mpg.example.com -dir . -service mpg -prefix mpg -nopw -ca

```

Take the generated *mpgcert_request.pem* file and sign it with the MyProxy CA. We will assume you're using SimpleCA in this documentation. You can substitute this with an accredited CA of your own as well.

```
#!bash
$ su - globus
$ grid-ca-sign -in mpgcert_request.pem -out mpgcert.pem
```

The resulting *mpgcert.pem* file should be saved along with the *mpgkey.pem* file created in the previous step. 

> Due to an outstanding bug in the jGlobus 2.x libraries, you will need to convert your key file to be consumable by OA4MP. Simply running the following command will do what you need. This is not part of setting up the trust relationship with MyProxy, it is simply mentioned here because once the relationship is set up, you will need to specify the cert and updated key paths in the *mpg.delegated.myproxy.hostcert* and *mpg.delegated.myproxy.hostkey* parameters of the OA4MP config file.
>
	`openssl rsa -in mpgkey.pem -outform PEM -out new-mpgkey.pem` 

## Configuring MyProxy trust relationship

Now that you have your service certificate and key, you can configure the MyProxy server to accept certificate requests from OA4MP via the myproxy-logon command by using the credentials created above. 

Updated your */etc/myproxy-server.conf* file, adding the subject of the certificate created above as the *authorized_retrievers* and *trusted_retrievers* value. **This is important. The subject DN of the X.509 certificate used by the OA4MP server to communicate with the MyProxy server must match this value**. If you do not know your certificate's subject, you can find it by running:

```
$!bash
$ grid-proxy-info -file mpgcert.pem -subject
```
Here is an example myproxy-server.conf file taken from a vanilla install of MyProxy and our above certificate:

```
authorized_retrievers 
"/O=Grid/OU=GlobusTest/OU=simpleCA/CN=mpg.example.com"
trusted_retrievers 
"/O=Grid/OU=GlobusTest/OU=simpleCA/CN=mpg.example.com"
certificate_issuer_cert /home/globus/.globus/simpleCA/cacert.pem
certificate_issuer_key  /home/globus/.globus/simpleCA/private/cakey.pem
certificate_serialfile  /home/globus/.globus/simpleCA/serial
certificate_out_dir     /home/globus/.globus/simpleCA/newcerts
certificate_mapapp      /etc/grid-security/grid-mapfile
certificate_serial_skip 5
cert_dir /etc/grid-security/certificates
max_cert_lifetime       9516
min_keylen              2048
```

Once you have updated, restart the MyProxy server.

## Testing the configuration

Now that we have set up the trust relationship, we can test out the setup by requesting a certificate using myproxy-logon. 

Let's say the X.509 certificate and key we created above are in /home/mpg/.globus/mpgcert.pem and /home/mpg/.globus/mpgkey.pem. You can test the configuration by running the following command:

```
#!bash
$ export X509_USER_CERT=/home/mpg/.globus/mpgcert.pem
$ export X509_USER_KEY=/home/mpg/.globus/mpgkey.pem
$ myproxy-logon -s myproxy.example.com -p 7512 -t 12 -l 'testuser' -S -o - -n 2>&1
```
> See the manpage of [myproxy-logon](http://grid.ncsa.illinois.edu/myproxy/man/myproxy-logon.1.html) for the meaning of the various command 
line switches. 

This command uses the local X.509 certificate and key we created above for 
authorized retrieval of a credential from the MyProxy server at 
myproxy.example.com on port 7512 with a lifetime of 12 hours and a 
username of 'testuser'.  The '-S -n' switches mean no password 
is used, while the '-o -' switches mean the credential is written to stdout.

## Troubleshooting


