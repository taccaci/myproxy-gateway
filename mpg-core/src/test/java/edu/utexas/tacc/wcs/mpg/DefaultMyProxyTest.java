/**
 * Copyright (c) 2014, Texas Advanced Computing Center
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the University of Texas at Austin nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */
package edu.utexas.tacc.wcs.mpg;

import java.io.IOException;
import java.util.Properties;
import java.util.UUID;

import org.apache.commons.lang.math.NumberUtils;
import org.testng.annotations.BeforeClass;

import edu.utexas.tacc.wcs.mpg.Settings;

public class DefaultMyProxyTest {
	
	protected Properties testProperties = new Properties();
	protected String testUsername;
	protected String testPassword;
	protected String testDelegatedUsername;
	protected String testDelegatedCredentialName;
	protected String testDelegatedPassword;
	protected String testDelegatedDn;
	
	protected String testImplicitHost;
	protected int testImplicitPort;
	protected String testImplicitUsername;
	protected String testImplicitDn;
	
	@BeforeClass
	public void beforeClass() throws IOException
	{
		testProperties.load(Settings.class.getClassLoader().getResourceAsStream("test.properties"));
		
		testUsername = (String)testProperties.get("mpg.test.username");
		testPassword = (String)testProperties.get("mpg.test.password");
		testDelegatedUsername = (String)testProperties.get("mpg.test.delegated.username");
		testDelegatedCredentialName = (String)testProperties.get("mpg.proxy.test.delegated.credentialname");
		testDelegatedPassword = UUID.randomUUID().toString().substring(16);
		testDelegatedDn = (String)testProperties.getProperty("mpg.test.delegated.dn", testUsername);
		
		testImplicitHost = (String)testProperties.get("mpg.test.implicit.host");
		testImplicitPort = NumberUtils.toInt((String)testProperties.get("mpg.test.implicit.port"), 7512);
		testImplicitUsername = (String)testProperties.getProperty("mpg.test.implicit.username", testUsername);
		testImplicitDn = (String)testProperties.getProperty("mpg.test.implicit.dn", testUsername);
	}

}
