/**
 * Copyright (c) 2014, Texas Advanced Computing Center
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the University of Texas at Austin nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */
package edu.utexas.tacc.wcs.mpg.common.auth.client;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.globus.gsi.CredentialException;
import org.globus.gsi.X509Credential;
import org.globus.gsi.gssapi.GlobusGSSCredentialImpl;
import org.globus.myproxy.CredentialInfo;
import org.globus.myproxy.MyProxyException;
import org.ietf.jgss.GSSCredential;
import org.ietf.jgss.GSSException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import edu.utexas.tacc.wcs.mpg.DefaultMyProxyTest;
import edu.utexas.tacc.wcs.mpg.Settings;
import edu.utexas.tacc.wcs.mpg.common.auth.client.DefaultMyProxyClient;
import edu.utexas.tacc.wcs.mpg.exceptions.MissingCredentialException;

public class DefaultMyProxyClientTest extends DefaultMyProxyTest 
{
	private DefaultMyProxyClient myproxyClient;
	private File tempCertDir;
	
	@BeforeClass
	public void beforeClass() throws IOException
	{
		super.beforeClass();
		
		myproxyClient = new DefaultMyProxyClient(Settings.DELEGATED_MYPROXY_HOST, Settings.DELEGATED_MYPROXY_PORT, testUsername, testPassword);
		
		tempCertDir = new File(FileUtils.getTempDirectoryPath(), "certificates-test-" + System.currentTimeMillis());

		tempCertDir.mkdirs();
		
		System.setProperty("X509_CERT_DIR", tempCertDir.getAbsolutePath());
	}
	
	@AfterClass
	public void afterClass() throws IOException
	{
		FileUtils.deleteDirectory(tempCertDir);
	}
	
	@Test
	public void bootstrapTrust() 
	{
		Assert.assertTrue(tempCertDir.exists(), "Temp cert directory does not exist.");
		
		try 
		{
			if (tempCertDir.list().length != 0) {
				FileUtils.deleteDirectory(tempCertDir);
				tempCertDir.mkdirs();
			}
			
			Map<String, String> trustedCertMap = myproxyClient.bootstrapTrust(false);
			Assert.assertFalse(trustedCertMap.isEmpty(), "No trust roots returned.");
			Assert.assertEquals(tempCertDir.list().length, 0, "Trust roots written to disk when told not to.");
		}
		catch (MyProxyException e) {
			Assert.fail("Unexpected exception bootstrapping trust.", e);
		} catch (IOException e) {
			Assert.fail("Failed to clear temp cert directory prior to test.", e);
		}
		
		try 
		{
			Map<String, String> trustedCertMap = myproxyClient.bootstrapTrust(true);
			
			Assert.assertFalse(trustedCertMap.isEmpty(), "No trust roots returned.");
			Assert.assertNotEquals(tempCertDir.list().length, 0, "Trust roots not written to disk when told to.");
		}
		catch (MyProxyException e) {
			Assert.fail("Unexpected exception bootstrapping trust.", e);
		}
	}
	
	@Test(dependsOnMethods={"bootstrapTrust"})
	public void getTrustroots() 
	{
		Assert.assertTrue(tempCertDir.exists(), "Temp cert directory does not exist.");
		
		try 
		{
			if (tempCertDir.list().length == 0) {
				myproxyClient.bootstrapTrust(true);
			}
			
			Map<String, String> trustedCertMap = myproxyClient.getTrustroots();
			Assert.assertFalse(trustedCertMap.isEmpty(), "No trust roots returned.");
		}
		catch (MyProxyException e) {
			Assert.fail("Unexpected exception retrieving trust roots.", e);
		}
	}

	@Test(dependsOnMethods = { "getTrustroots" })
	public void getCredentialForUser() {
		try 
		{
			if (tempCertDir.list().length == 0) {
				myproxyClient.bootstrapTrust(true);
			}
			
			GSSCredential cred = myproxyClient.getCredentialForUser(null, null, 86400, null, null);
			Assert.assertNotNull(cred, "No default credential retrieved from myproxy");
			Assert.assertEquals(((GlobusGSSCredentialImpl)cred).getX509Credential().getIdentity(),
					testDelegatedDn,
					"Subject of returned credential does not match the expected identity.");
		}
		catch (MyProxyException e) 
		{
			e.printStackTrace();
			Assert.fail("Failed to retrieve credential.", e);
		}
		
	}	

	@Test(dependsOnMethods = { "getCredentialForUser" })
	public void serializeCredential() 
	{
		InputStream is = null;
		try 
		{
			if (tempCertDir.list().length == 0) {
				myproxyClient.bootstrapTrust(true);
			}
			
			GSSCredential cred = myproxyClient.getCredentialForUser(null, null, 86400, null, null);
			Assert.assertNotNull(cred, "No default credential retrieved from myproxy");
			
			// serialize credentail
			String serializedCredential = myproxyClient.serializeCredential(cred);
			
			// no try to deserialize to make sure it's still good.
			is = new ByteArrayInputStream(serializedCredential.getBytes());
			X509Credential globusCred = new X509Credential(is);
			new GlobusGSSCredentialImpl(globusCred, GSSCredential.INITIATE_AND_ACCEPT);
		} 
		catch (CredentialException e) 
		{
			Assert.fail("Failed to deserialied credential. Bad serialization", e);
		}
		catch (GSSException e) 
		{
			Assert.fail("Failed to retrieve credential.", e);
		} 
		catch (MyProxyException e) 
		{
			Assert.fail("Failed to retrieve credential.", e);
		}
		finally {
			try {is.close();} catch (Exception e) {}
		}
	}

	@Test(dependsOnMethods = { "getCredentialForUser" })
	public void storeCredentialForUser()//String authUsername, String delegatedUsername, String credentialName) 
	{
		GSSCredential cred = null;
		try 
		{
			if (tempCertDir.list().length == 0) {
				myproxyClient.bootstrapTrust(true);
			}
			
			cred = myproxyClient.storeCredentialForUser(null, testDelegatedUsername, testDelegatedPassword, testDelegatedCredentialName, 86400, null, null, null, null);
			Assert.assertNotNull(cred, "Credential was not stored");
		} 
		catch (MyProxyException e) {
			Assert.fail("Failed to store a credential for the user", e);
		}
		catch (GSSException e) {
			Assert.fail("Failed to store a credential for the user", e);
		}
		
		try {
			DefaultMyProxyClient delegatedMyproxyClient = new DefaultMyProxyClient(Settings.DELEGATED_MYPROXY_HOST, Settings.DELEGATED_MYPROXY_PORT, testDelegatedUsername, testDelegatedPassword);
			GSSCredential storedCred = delegatedMyproxyClient.getCredentialForUser(null, testDelegatedCredentialName, 86400, null, null);
			Assert.assertNotNull(storedCred, "No stored credential retrieved from myproxy using stored username and password");
		} 
		catch (MyProxyException e) {
			Assert.fail("Failed to store a credential from myproxy using stored username and password", e);
		}
		
		try {
			GSSCredential delegatedCred = myproxyClient.getCredentialForUser(testDelegatedUsername, testDelegatedCredentialName, 86400, null, null);
			Assert.assertNotNull(delegatedCred, "No stored credential retrieved from myproxy using owner's username and password");
		} 
		catch (MyProxyException e) {
			Assert.fail("Failed to retrieve credential from myproxy using owner's username and password", e);
		}
	}
	
	@Test(dependsOnMethods = { "storeCredentialForUser" })
	public void getAllUserCredentails()
	{
		try 
		{
			if (tempCertDir.list().length == 0) {
				myproxyClient.bootstrapTrust(true);
			}
			
			GSSCredential cred = myproxyClient.storeCredentialForUser(null, testDelegatedUsername, testDelegatedPassword, testDelegatedCredentialName, 86400, null, null, null, null);
			Assert.assertNotNull(cred,"Credential storing failed");
			
			CredentialInfo[] credentials = myproxyClient.getAllUserCredentails(testDelegatedUsername);
			Assert.assertEquals(credentials.length, 1, "Unexpected number of credentials returned.");
		} 
		catch (MyProxyException e) 
		{
			Assert.fail("Failed to retrieve credentials", e);
		} 
		catch (GSSException e) {
			Assert.fail("Failed to store credentials", e);
		}
	}
	
	@Test(dependsOnMethods={"getAllUserCredentails"})
	public void deleteCredential() 
	{
		GSSCredential cred = null;
		try 
		{
			if (tempCertDir.list().length == 0) {
				myproxyClient.bootstrapTrust(true);
			}
			
			cred = myproxyClient.storeCredentialForUser(null, testDelegatedUsername, testDelegatedPassword, testDelegatedCredentialName, 86400, null, null, null, null);
			Assert.assertNotNull(cred, "Credential was not stored");
		} 
		catch (MyProxyException e) {
			Assert.fail("Failed to store a credential for the user", e);
		}
		catch (GSSException e) {
			Assert.fail("Failed to store a credential for the user", e);
		}
		
		try
		{
			myproxyClient.deleteCredential(testDelegatedUsername, testDelegatedCredentialName);
		} catch (MissingCredentialException e) {
			Assert.fail("No credential named " + testDelegatedCredentialName + " found on myproxy server for user " + testDelegatedUsername, e);
		} catch (MyProxyException e) {
			Assert.fail("Failed to delete the stored credential");
		}
		
		try {
			CredentialInfo[] credentials = myproxyClient.getAllUserCredentails(testDelegatedUsername);
			Assert.assertEquals(credentials.length, 0, "Stored credential found after it was deleted.");
		} 
		catch (MyProxyException e) {
			Assert.fail("Failed to retrieve information on stored credentials. Cannot verify deletion.", e);
		}
	}	
}
