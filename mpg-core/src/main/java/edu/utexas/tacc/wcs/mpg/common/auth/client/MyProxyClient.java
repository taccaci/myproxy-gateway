/**
 * Copyright (c) 2014, Texas Advanced Computing Center
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the University of Texas at Austin nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */
package edu.utexas.tacc.wcs.mpg.common.auth.client;

import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;

import org.globus.myproxy.CredentialInfo;
import org.globus.myproxy.MyProxyException;
import org.ietf.jgss.GSSCredential;
import org.ietf.jgss.GSSException;

import edu.utexas.tacc.wcs.mpg.exceptions.MissingCredentialException;

public interface MyProxyClient {

	/**
	 * Serializes a GSSCredential into a string that can be persisted to disk
	 * or passed back to the user in a JSON response.
	 * @param GSSCredential to be serialized
	 * @return String representation of a GSSCredential
	 * @throws GSSException
	 */
	public String serializeCredential(GSSCredential cred) throws GSSException;
	
	/**
	 * Retrieves trustroot information from the MyProxy server.
	 *
	 * @param  credential 
	 *         The local GSI credentials to use for authentication.
	 *         Can be set to null if no local credentials.
	 * @param  params
	 *         The parameters for the get-trustroots operation.
	 * @exception MyProxyException
	 *         If an error occurred during the operation.
	 */
	public abstract Map<String, String> getTrustroots() throws MyProxyException;

	/**
	 * Retrieves host cert for the myproxy server.
	 *
	 * @exception MyProxyException
	 *         If an error occurred during the operation.
	 */
	public abstract Map<String, String> bootstrapTrust(boolean writeToDisk)
			throws MyProxyException;

	public abstract void writeBootstrapedTrust(X509Certificate[] acceptedIssuers)
			throws MyProxyException;

	/**
	 * Retrieves a credential for another uses relying on the trust relationship
	 * from with the remote MyProxy Server.
	 * 
	 * @param authenticatedUsername
	 * @return delegated GSSCredential for the named user. 
	 * @throws MyProxyException 
	 */
	public abstract GSSCredential getCredentialForUser(String username,
			String credentialName, int lifetime, List<String> vomses,
			List<String> voname) throws MyProxyException;

	public abstract CredentialInfo[] getAllUserCredentails(String username)
			throws MyProxyException;

	/**
	 * Retrieves a credential for another uses relying on the trust relationship
	 * from with the remote MyProxy Server.
	 * 
	 * @param authenticatedUsername
	 * @return delegated GSSCredential for the named user. 
	 * @throws MyProxyException 
	 * @throws GSSException 
	 */
	public abstract GSSCredential storeCredentialForUser(String username,
			String delegatedUsername, String delegatedCredentialPassword,
			String delegatedCredentialName, int delegatedLifetime,
			String delegatedCredentialDescription, String delgatedRenewer,
			String delegatedRetriever, String trustedRetriever)
			throws MyProxyException, GSSException;

	public abstract void deleteCredential(String username, String credentialName)
			throws MissingCredentialException, MyProxyException;

}