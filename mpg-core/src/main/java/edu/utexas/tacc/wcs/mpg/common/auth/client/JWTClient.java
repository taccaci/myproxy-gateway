/**
 * Copyright (c) 2014, Texas Advanced Computing Center
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the University of Texas at Austin nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */
package edu.utexas.tacc.wcs.mpg.common.auth.client;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;

import net.minidev.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.testng.Assert;

import com.nimbusds.jwt.ReadOnlyJWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

/**
 * @author rion1
 *
 */
public class JWTClient 
{
	private static final Logger log = Logger.getLogger(JWTClient.class);
	
//	private static final String IDENTITY_STORE_PUBLIC_KEY_FILE = "jwtcert.pem";
	private static final ThreadLocal<JSONObject> threadJWTPayload = new ThreadLocal<JSONObject>();

//	private static RSAPublicKey identityServerPublicKey;
//	
//	public static RSAPublicKey getIdentityServerPublicKey()
//	throws Exception 
//	{
//		if (identityServerPublicKey == null) 
//		{
//			InputStream is = null;
//			try 
//			{
//				Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
//				
//				CertificateFactory cf = CertificateFactory.getInstance("X509", "BC");
//				is = Thread.currentThread().getContextClassLoader().getResourceAsStream(IDENTITY_STORE_PUBLIC_KEY_FILE);
//				X509Certificate certificate = (X509Certificate) cf.generateCertificate(is);
//				identityServerPublicKey = (RSAPublicKey)certificate.getPublicKey();
//			} 
//			finally {
//				try { is.close(); } catch (Exception e) {}
//			}
//		}
//		
//		return identityServerPublicKey;
//	}
//	
	public static boolean parse(String serializedToken, String tenantId)
	{
		if (tenantId == null) return false;
		
		try 
		{
			// Create HMAC signer
			SignedJWT signedJWT = SignedJWT.parse(serializedToken);
			
			if (signedJWT != null)
			{
				ReadOnlyJWTClaimsSet claims = signedJWT.getJWTClaimsSet();
				
				Date expirationDate = claims.getExpirationTime();
				
				Assert.assertNotNull(expirationDate, 
						"No expiration date in the JWT header. Authentication failed.");
				
				Assert.assertTrue(expirationDate.after(new Date()), 
						"JWT has expired. Authentication failed.");
				
				JSONObject json = claims.toJSONObject();
				
				tenantId = StringUtils.lowerCase(tenantId);
				tenantId = StringUtils.replaceChars(tenantId, '_', '.');
				tenantId = StringUtils.replaceChars(tenantId, '-', '.');
				json.put("tenantId", tenantId);
				
				threadJWTPayload.set(json);
				
				Assert.assertNotNull(getCurrentEndUser(), 
						"No end user specified in the JWT header. Authentication failed.");
				
				Assert.assertNotNull(getCurrentTenant(), 
						"No tenant specified in the JWT header. Authentication failed.");
				
				Assert.assertNotNull(getCurrentSubscriber(), 
						"No subscriber specified in the JWT header. Authentication failed.");
				
				return true;
			}
			else
			{
				log.error("Invalid JWT signature.");
				return false;
			} 
		} catch (ParseException e) {
			log.error("Failed to parse JWT object. Authentication failed.", e);
			return false;
		} catch (Throwable e) {
			log.error("Error processing JWT header. Authentication failed.", e);
			return false;
		}
	}
	
	public static JSONObject getCurrentJWSObject()
	{
		return threadJWTPayload.get();
	}
	
	public static String getCurrentSubscriber() 
	{
		try {
			JSONObject claims = getCurrentJWSObject();
			return (String)claims.get("http://wso2.org/claims/subscriber");
		} catch (Exception e) {
			return null;
		}
	}
	
	public static String getCurrentTenant()
	{
		try 
		{
			JSONObject claims = getCurrentJWSObject();
			if (claims.containsKey("tenantId") || !StringUtils.isEmpty((String)claims.get("tenantId"))) {
				return (String)claims.get("tenantId");
			}
			else
			{
				String subscriber = (String)claims.get("http://wso2.org/claims/subscriber");
				if (StringUtils.countMatches(subscriber, "@") > 0) {
					return subscriber.substring(subscriber.lastIndexOf("@") + 1);
				} else {
					return (String)claims.get("http://wso2.org/claims/enduserTenantId");
				}
			}
		} catch (Exception e) {
			return null;
		}
	}

	public static String getCurrentEndUser() 
	{
		try {
			JSONObject claims = getCurrentJWSObject();
			String tenant = getCurrentTenant();
			String endUser = (String)claims.get("http://wso2.org/claims/enduser");
			endUser = StringUtils.replace(endUser, "@carbon.super", "");
			if (StringUtils.endsWith(endUser, tenant)) {
				return StringUtils.substring(endUser, 0, (-1 * tenant.length() - 1));
			} else if (endUser.contains("@")){
				return StringUtils.substringBefore(endUser, "@");
			} else if (endUser.contains("/")){
				return StringUtils.substringAfter(endUser, "/");
			} else {
				return endUser;
			}
		} catch (Exception e) {
			return null;
		}
	}
	public static boolean isTenantAdmin() {
		try {
			JSONObject claims = getCurrentJWSObject();
			String roles = (String)claims.get("http://wso2.org/claims/role");
			if (!StringUtils.isEmpty(roles)) {
				return Arrays.asList(StringUtils.split(roles, ",")).contains("admin");
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	public static boolean isSuperAdmin() {
		return false;
	}
	
}
