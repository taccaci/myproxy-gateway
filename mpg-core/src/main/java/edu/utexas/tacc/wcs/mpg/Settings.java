/**
 * Copyright (c) 2014, Texas Advanced Computing Center
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the University of Texas at Austin nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */
package edu.utexas.tacc.wcs.mpg;

import java.io.File;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

/**
 * Global settings for the service. Read in from service.properties
 * @author dooley
 *
 */
public class Settings
{
	private static final String PROPERTY_FILE = "service.properties";

	/* Service debug parameters */
	public static boolean 						DEBUG;
	public static String 						DEBUG_USERNAME;
	public static String						API_VERSION;
	public static String 						SERVICE_BASE_URL;
	public static Integer						DEFAULT_CREDENTIAL_LIFETIME;

	public static int 							JETTY_PORT;
	public static int 							JETTY_AJP_PORT;

	/* Service auth settings */
	public static String 						AUTH_TYPE;

	/* LDAP Auth type settings */
	public static String 						LDAP_URL;
	public static String 						LDAP_BASE_DN;
	public static String 						LDAP_KEYSTORE_PATH;
	public static String 						LDAP_TRUSTSTORE_PATH;

	/* WSO2 Auth type settings */
	public static String 						LOGIN_JWT_HEADER;

	/* MyProxy Auth type settings */
	public static String 						LOGIN_MYPROXY_HOST;
	public static int 							LOGIN_MYPROXY_PORT;

	/* Delegated MyProxy settings */
	public static String 						DELEGATED_MYPROXY_MODE;
	public static String 						DELEGATED_MYPROXY_HOST;
	public static int 							DELEGATED_MYPROXY_PORT;
	public static String 						DELEGATED_MYPROXY_HOSTKEY;
	public static String 						DELEGATED_MYPROXY_HOSTCERT;
	public static String 						DELEGATED_MYPROXY_HOST_PASSWORD;

	/* Mail server settings */
	public static String 						MAIL_SERVER;
	public static String 						MAILSMTPSPROTOCOL;
	public static String 						MAILLOGIN;
	public static String 						MAILPASSWORD;

	private static String DELEGATED_MYPROXY_HOSTKEY_PASS;

	static
	{
		Properties props = new Properties();
		try {
			props.load(Settings.class.getClassLoader().getResourceAsStream(PROPERTY_FILE));
			for(Object key: props.keySet())
			{	String envName = StringUtils.replace(StringUtils.upperCase(key.toString()), ".", "_");
				String environmentPropertyValue = System.getProperty(envName);

				if (StringUtils.isNotEmpty(environmentPropertyValue)) {
					props.put(key, environmentPropertyValue);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		/* Basic service settings */
		DEBUG = Boolean.valueOf((String)props.get("mpg.debug.mode"));
		DEBUG_USERNAME = (String)props.getProperty("mpg.debug.username");

		API_VERSION = (String)props.getProperty("mpg.api.version");

		JETTY_PORT = NumberUtils.toInt((String)props.get("mpg.jetty.port"), 8182);
		JETTY_AJP_PORT = NumberUtils.toInt((String)props.get("mpg.jetty.ajp.port"), 8183);

		/* SSL keystore settings */
		SERVICE_BASE_URL = (String)props.getProperty("mpg.base.url", "http://localhost:8080/myproxy/");
		if (!SERVICE_BASE_URL.endsWith("/")) SERVICE_BASE_URL += "/";

		DEFAULT_CREDENTIAL_LIFETIME = NumberUtils.toInt((String)props.get("mpg.default.lifetime"), 43200);


		/* Delegated myproxy server settings. These are read first because the auth myproxy
		 * settings default to these settings if not explicitly provided.*/

		DELEGATED_MYPROXY_MODE = (String)props.getProperty("mpg.delegated.myproxy.mode", "none");
		DELEGATED_MYPROXY_HOST = (String)props.getProperty("mpg.delegated.myproxy.host", "localhost");
		DELEGATED_MYPROXY_PORT = NumberUtils.toInt((String)props.getProperty("mpg.delegated.myproxy.port"), 7512);
		DELEGATED_MYPROXY_HOSTKEY_PASS = (String) props.getProperty("mpg.delegated.myproxy.hostkey.pass");

		DELEGATED_MYPROXY_HOSTKEY = (String) props.getProperty("mpg.delegated.myproxy.hostkey", System.getProperty("X509_USER_KEY"));
		if (StringUtils.isEmpty(DELEGATED_MYPROXY_HOSTKEY)) {
			if (new File("/etc/grid-security/hostkey.pem").exists()) {
				DELEGATED_MYPROXY_HOSTKEY = "/etc/grid-security/hostkey.pem";
			} else if (new File(System.getProperty("GLOBUS_LOCATION") + "/etc/grid-security/hostkey.pem").exists()) {
				DELEGATED_MYPROXY_HOSTKEY = System.getProperty("GLOBUS_LOCATION") + "/etc/grid-security/hostkey.pem";
			} else if (new File(System.getProperty("user.home") + "/.globus/hostkey.pem").exists()) {
				DELEGATED_MYPROXY_HOSTKEY = System.getProperty("user.home") + "/.globus/hostkey.pem";
			}
		}

		DELEGATED_MYPROXY_HOSTCERT = (String) props.getProperty("mpg.delegated.myproxy.hostcert", System.getProperty("X509_USER_CERT"));
		if (StringUtils.isEmpty(DELEGATED_MYPROXY_HOSTKEY)) {
			if (new File("/etc/grid-security/hostcert.pem").exists()) {
				DELEGATED_MYPROXY_HOSTCERT = "/etc/grid-security/hostcert.pem";
			} else if (new File(System.getProperty("GLOBUS_LOCATION") + "/etc/grid-security/hostcert.pem").exists()) {
				DELEGATED_MYPROXY_HOSTCERT = System.getProperty("GLOBUS_LOCATION") + "/etc/grid-security/hostcert.pem";
			} else if (new File(System.getProperty("user.home") + "/.globus/hostcert.pem").exists()) {
				DELEGATED_MYPROXY_HOSTCERT = System.getProperty("user.home") + "/.globus/hostcert.pem";
			}
		}


		/* Authentication parameters for communicating with the service */
		AUTH_TYPE = (String) props.getProperty("mpg.auth.type", "none");

		LDAP_URL = (String)props.getProperty("mpg.auth.ldap.url");
		LDAP_BASE_DN = (String)props.getProperty("mpg.auth.ldap.base.dn");
		LDAP_KEYSTORE_PATH = props.getProperty("mpg.auth.ldap.keystore", System.getenv("user.home") + "/jssecacerts");
		LDAP_TRUSTSTORE_PATH = props.getProperty("mpg.auth.ldap.truststore", System.getenv("user.home") + "/jssecacerts");

		LOGIN_JWT_HEADER = (String)props.getProperty("mpg.auth.wso2.jwt.header", "x-jwt-assertion");
		LOGIN_JWT_HEADER = StringUtils.lowerCase(LOGIN_JWT_HEADER);

		LOGIN_MYPROXY_HOST = (String)props.getProperty("mpg.auth.myproxy.host", DELEGATED_MYPROXY_HOST);
		LOGIN_MYPROXY_PORT = NumberUtils.toInt((String)props.getProperty("mpg.auth.myproxy.port"), DELEGATED_MYPROXY_PORT);


		/* Mail settings for sending error messages, etc */
		MAIL_SERVER = (String) props.getProperty("mail.smtps.host", "localhost");
		MAILSMTPSPROTOCOL = (String) props.getProperty("mail.smtps.auth", "false");
		MAILLOGIN = (String) props.getProperty("mail.smtps.user", System.getProperty("user.name"));
		MAILPASSWORD = (String) props.getProperty("mail.smtps.passwd");
	}
}
