/**
 * Copyright (c) 2014, Texas Advanced Computing Center
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the University of Texas at Austin nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */
package edu.utexas.tacc.wcs.mpg.common.auth.client;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.log4j.Logger;

import edu.utexas.tacc.wcs.mpg.Settings;

public class LdapClient {
	private static Logger log = Logger.getLogger(LdapClient.class);
	private String username;
	private String pass;

	/**
	 * Create a new ldap instance.
	 * 
	 * @param username binding username
	 * @param pass binding password
	 */
	public LdapClient(String username, String pass) 
	{
		this.username = username;
		this.pass = pass;
	}

	/**
	 * Sets up SSL and the environment needed to communicate with 
	 * the remote ldap server.
	 * 
	 * @return DirContext used to communicate with LDAP.
	 */
	private DirContext getDirContext() 
	{
		System.setProperty("javax.net.ssl.keyStore", Settings.LDAP_KEYSTORE_PATH);
		System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
		System.setProperty("javax.net.ssl.trustStore", Settings.LDAP_TRUSTSTORE_PATH);
		System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, Settings.LDAP_URL
				+ Settings.LDAP_BASE_DN);

		// Specify SSL
		env.put(Context.SECURITY_PROTOCOL, "ssl");
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, "uid=" + username + ","
				+ Settings.LDAP_BASE_DN);
		env.put(Context.SECURITY_CREDENTIALS, pass);

		try {
			return new InitialDirContext(env);
		} catch (NamingException e) {
			log.error(e.getMessage(), e.getCause());
		}
		return null;
	}

	/**
	 * Attempt to bind to the ldap with the credentials provided
	 * in the constructor
	 * 
	 * @return true on success. Failure otherwise
	 */
	public boolean login() 
	{
		DirContext ctx = getDirContext();
		if (ctx == null) {
			return false;
		} else {
			try { ctx.close(); } catch (Exception e) {}
		}
		return true;
	}

	/**
	 * Returns all attributes for the authenticated username
	 * @return Attributes
	 */
	public Attributes getAttributes() 
	{
		return getAttributes(username);
	}

	/**
	 * Retrives all attributes for the given user from ldap
	 * 
	 * @param user
	 * @return Attributes
	 */
	public Attributes getAttributes(String user) 
	{
		DirContext ctx = getDirContext();

		/*
		 * fail to connect to LDAP
		 */
		if (ctx == null)
			return null;

		NamingEnumeration<SearchResult> results = null;
		try 
		{
			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
			results = ctx.search("", "uid=" + user, controls);
			//return attribute if found
			return results.hasMore()?results.next().getAttributes():null;
		} 
		catch (Exception e) 
		{
			log.error(e.getMessage(), e.getCause());			
		} 
		finally 
		{
			if (results != null)  {
				try { results.close(); } catch (Exception e) {}
			}
			
			if (ctx != null) {
				try { ctx.close(); } catch (Exception e) {}
			}
		}
		return null;
	}

	/**
	 * Queries for the given attributes in the bound ldap server.
	 * 
	 * @param attr
	 * @param searchTerm
	 * @return
	 */
	public List<Attributes> searchDirectory(String attr, String searchTerm) 
	{
		DirContext ctx = getDirContext();
		List<Attributes> results = null;

		/*
		 * fail to connect to LDAP
		 */
		if (ctx == null)
			return null;

		NamingEnumeration<SearchResult> answers = null;
		try 
		{
			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
			String filter = attr + "=*" + searchTerm + "*";
		
			answers = ctx.search("", filter, controls);
			while (answers.hasMore()) {
				// initial result only has search result
				if (results == null)
					results = new ArrayList<Attributes>();
				results.add(answers.next().getAttributes());
			}
			
			return results;
		} 
		catch (Exception e) 
		{
			log.error(e.getMessage(), e.getCause());
		} 
		finally 
		{
			if (answers != null) {
				try { answers.close(); } catch (Exception e) {}
			}
			
			if (ctx != null) {
				try { ctx.close(); } catch (Exception e) {}
			}
		}
		return null;
	}
}
