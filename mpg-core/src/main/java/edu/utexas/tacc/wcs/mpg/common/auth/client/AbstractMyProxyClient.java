/**
 * Copyright (c) 2014, Texas Advanced Computing Center
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the University of Texas at Austin nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */
/**
 * 
 */
package edu.utexas.tacc.wcs.mpg.common.auth.client;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.security.auth.x500.X500Principal;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.globus.gsi.CertUtil;
import org.globus.gsi.gssapi.GlobusGSSCredentialImpl;
import org.globus.myproxy.MyProxy;
import org.globus.myproxy.MyProxyException;
import org.globus.myproxy.MyTrustManager;
import org.gridforum.jgss.ExtendedGSSCredential;
import org.ietf.jgss.GSSCredential;
import org.ietf.jgss.GSSException;

import edu.utexas.tacc.wcs.mpg.Settings;

/**
 * @author dooley
 *
 */
@SuppressWarnings("deprecation")
public abstract class AbstractMyProxyClient implements MyProxyClient 
{
	private static final Logger logger = Logger.getLogger(DefaultMyProxyClient.class);
	
	public abstract String getHost();
	public abstract void setHost(String host);
	public abstract int getPort();
	public abstract void setPort(int port);
	
	/* (non-Javadoc)
	 * @see edu.utexas.tacc.wcs.oa4mp.common.auth.client.MyProxyClient#serializeCredential(org.ietf.jgss.GSSCredential)
	 */
	@Override
	public String serializeCredential(GSSCredential cred) throws GSSException 
	{
		byte[] serializedCredential = ((GlobusGSSCredentialImpl)cred).export(ExtendedGSSCredential.IMPEXP_OPAQUE);
		return new String(serializedCredential);
	}
	
	/* (non-Javadoc)
	 * @see edu.utexas.tacc.wcs.oa4mp.common.auth.client.MyProxyClient#bootstrapTrust(boolean)
	 */
    @Override
	public Map<String,String> bootstrapTrust(boolean writeToDisk) throws MyProxyException 
    {
        try 
        {
        	Map<String, String> trustCertMap = new HashMap<String,String>();
        	
            SSLContext sc = SSLContext.getInstance("SSL");
            MyTrustManager myTrustManager = new MyTrustManager();
            TrustManager[] trustAllCerts = new TrustManager[] { myTrustManager };
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            SSLSocketFactory sf = sc.getSocketFactory();
            SSLSocket socket = (SSLSocket)sf.createSocket(getHost(), getPort());
            socket.setEnabledProtocols(new String[] { "SSLv3", "SSLv2Hello", "TLSv1" });
            socket.startHandshake();
            socket.close();

            X509Certificate[] acceptedIssuers = myTrustManager.getAcceptedIssuers();
            
            if (acceptedIssuers == null) {
                throw new MyProxyException("Failed to determine MyProxy server trust roots in bootstrapTrust.");
            }
            
            for (int idx = 0; idx < acceptedIssuers.length; idx++)
            {   
                StringBuffer newSubject = new StringBuffer();
                String[] subjArr = acceptedIssuers[idx].getSubjectDN().getName().split(", ");
                for(int i = (subjArr.length - 1); i > -1; i--)
                {
                    newSubject.append("/");
                    newSubject.append(subjArr[i]);
                }
                String subject = newSubject.toString();
                String hash = opensslHash(acceptedIssuers[idx]);
                
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                CertUtil.writeCertificate(os, acceptedIssuers[idx]);
                
                os.close();
                
                trustCertMap.put(hash + ".0", new String(os.toByteArray()));
                
                logger.debug("wrote trusted certificate " + hash + ".0");
                
                StringBuilder wr = new StringBuilder();
                wr.append("access_id_CA X509 '");
                wr.append(subject);
                wr.append("'\npos_rights globus CA:sign\ncond_subjects globus \"*\"\n");
                
                trustCertMap.put(hash + ".signing_policy", wr.toString());
                
                logger.debug("wrote trusted certificate policy " + hash + ".signing_policy");
            }
            
            if (writeToDisk) {
            	writeBootstrapedTrust(acceptedIssuers);
            }
            	
            return trustCertMap;
        } 
        catch(Exception e) 
        {e.printStackTrace();
            throw new MyProxyException("MyProxy bootstrapTrust failed.", e);
        }
    }
    
    /* (non-Javadoc)
	 * @see edu.utexas.tacc.wcs.oa4mp.common.auth.client.MyProxyClient#writeBootstrapedTrust(java.security.cert.X509Certificate[])
	 */
    @Override
	public void writeBootstrapedTrust(X509Certificate[] acceptedIssuers) throws MyProxyException {
        try {
            
            if (acceptedIssuers == null) {
                throw new MyProxyException("Failed to determine MyProxy server trust roots in bootstrapTrust.");
            }
            for (int idx = 0; idx < acceptedIssuers.length; idx++)
            {
                File x509Dir = new File(MyProxy.getTrustRootPath());
                if (!x509Dir.exists())
                {
                	x509Dir.mkdirs();
                }
                
                StringBuffer newSubject = new StringBuffer();
                String[] subjArr = acceptedIssuers[idx].getSubjectDN().getName().split(", ");
                for(int i = (subjArr.length - 1); i > -1; i--)
                {
                    newSubject.append("/");
                    newSubject.append(subjArr[i]);
                }
                String subject = newSubject.toString();

                File tmpDir = new File(MyProxy.getTrustRootPath() + "-" +
                                       System.currentTimeMillis());
                if (tmpDir.mkdir() == true)
                {
                    String hash = opensslHash(acceptedIssuers[idx]);
                    String filename = tmpDir.getPath() + File.separator + hash + ".0";

                    FileOutputStream os = new FileOutputStream(new File(filename));
                    CertUtil.writeCertificate(os, acceptedIssuers[idx]);

                    os.close();
                    if (logger.isDebugEnabled()) {
                        logger.debug("wrote trusted certificate to " + filename);
                    }

                    filename = tmpDir.getPath() + File.separator + hash + ".signing_policy";

                    os = new FileOutputStream(new File(filename));
                    Writer wr = new OutputStreamWriter(os, Charset.forName("UTF-8"));
                    wr.write("access_id_CA X509 '");
                    wr.write(subject);
                    wr.write("'\npos_rights globus CA:sign\ncond_subjects globus \"*\"\n");
                    wr.flush();
                    wr.close();
                    os.close();

                    if (logger.isDebugEnabled()) {
                        logger.debug("wrote trusted certificate policy to " + filename);
                    }

                    // success.  commit the bootstrapped directory.
                    try {
                    	FileUtils.copyDirectory(tmpDir, x509Dir, true);
                    	if (logger.isDebugEnabled()) {
                            logger.debug("renamed " + tmpDir.getPath() + " to " +
                                         x509Dir.getPath());
                        }
                    } catch (IOException e) {
                    	throw new MyProxyException("Unable to rename " + tmpDir.getPath() + " to " +
                                x509Dir.getPath());
                    }
                }
                else
                {
                    throw new MyProxyException("Cannot create temporary directory: " + tmpDir.getName());
                }
            }
        } catch(Exception e) {
            throw new MyProxyException("MyProxy bootstrapTrust failed.", e);
        }
    }
    
//    private String readLine(InputStream is) throws IOException {
//        StringBuffer sb = new StringBuffer();
//        for (int c = is.read(); c > 0 && c != '\n'; c = is.read()) {
//            sb.append((char) c);
//        }
//        if (sb.length() > 0) {
//            if (logger.isDebugEnabled()) {
//                logger.debug("Received line: " + sb);
//            }
//            return new String(sb);
//        }
//        return null;
//    }
    
	/*
		the following methods are based off code to compute the subject
		name hash from:
		http://blog.piefox.com/2008/10/javaopenssl-ca-generation.html
	*/
	private String opensslHash(X509Certificate cert) 
	{
		try {
			return openssl_X509_NAME_hash(cert.getSubjectX500Principal());
		}
		catch (Exception e) {
			throw new Error("MD5 isn't available!", e);
		}
	}
	
	/**
	* Generates a hex X509_NAME hash (like openssl x509 -hash -in cert.pem)
	* Based on openssl's crypto/x509/x509_cmp.c line 321
	*/
	private String openssl_X509_NAME_hash(X500Principal p) throws Exception 
	{
		// This code replicates OpenSSL's hashing function
		// DER-encode the Principal, MD5 hash it, then extract the first 4 bytes and reverse their positions
		byte[] derEncodedSubject = p.getEncoded();
		byte[] md5 = MessageDigest.getInstance("MD5").digest(derEncodedSubject);
	
		// Reduce the MD5 hash to a single unsigned long
		byte[] result = new byte[] { md5[3], md5[2], md5[1], md5[0] };
		return toHex(result);
	}
	
	// encode binary to hex
	private String toHex(final byte[] bin) 
	{
		if (bin == null || bin.length == 0)
			return "";

		char[] buffer = new char[bin.length * 2];

		final char[] hex = "0123456789abcdef".toCharArray();

		// i tracks input position, j tracks output position
		for (int i = 0, j = 0; i < bin.length; i++)
		{
			final byte b = bin[i];
			buffer[j++] = hex[(b >> 4) & 0x0F];
			buffer[j++] = hex[b & 0x0F];
		}
		return new String(buffer);
	}

}
