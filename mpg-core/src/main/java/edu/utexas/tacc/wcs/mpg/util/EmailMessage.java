/**
 * Copyright (c) 2014, Texas Advanced Computing Center
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the University of Texas at Austin nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */
package edu.utexas.tacc.wcs.mpg.util;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import com.sun.mail.smtp.SMTPTransport;

import edu.utexas.tacc.wcs.mpg.Settings;
import edu.utexas.tacc.wcs.mpg.exceptions.NotificationException;

/**
 * Simple email class using the javamail API to send an email.
 * 
 * @author Rion Dooley < dooley [at] tacc [dot] utexas [dot] edu >
 *
 */
public class EmailMessage {
    
    public static Logger log = Logger.getLogger(EmailMessage.class.getName());
    
    public static void send(String name, String address, String subject, String body) throws NotificationException {
        Session session = null;
        
        Properties props = new Properties();
        
        try 
        {
            props.put("mail.smtp.host", Settings.MAIL_SERVER);
            props.put("mail.smtp.auth", Settings.MAILPASSWORD);
            
            session = Session.getInstance(props);
            
            MimeMessage message = createMessageObject(session, 
                    subject, body, name, address);
            
            SMTPTransport transport = (SMTPTransport)session.getTransport("smtp");
            
            transport.connect(Settings.MAIL_SERVER,Settings.MAILLOGIN, Settings.MAILPASSWORD);
            transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
            
        } catch (Exception e) {
            
            log.error(e);
            throw new NotificationException("Email notification failed.",e);
            
        }
    }
   
    private static MimeMessage createMessageObject(Session session, 
                                                    String subject, 
                                                    String body, 
                                                    String name,
                                                    String address) 
    throws Exception {
        
        MimeMessage message = new MimeMessage(session);
        
        message.setText(body);
        
        message.setSubject(subject);
        
        Address fromAddress = new InternetAddress(Settings.MAILLOGIN, "Agave Notification Service");
        
        Address toAddress = new InternetAddress(address, name);
        
        message.setFrom(fromAddress);
        
        message.setRecipient(Message.RecipientType.TO, toAddress);
        
        return message;
    }    
}