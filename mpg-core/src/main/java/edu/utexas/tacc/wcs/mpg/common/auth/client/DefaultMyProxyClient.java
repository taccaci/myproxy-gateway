/**
 * Copyright (c) 2014, Texas Advanced Computing Center
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the University of Texas at Austin nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */
package edu.utexas.tacc.wcs.mpg.common.auth.client;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.globus.myproxy.CredentialInfo;
import org.globus.myproxy.DestroyParams;
import org.globus.myproxy.GetParams;
import org.globus.myproxy.InfoParams;
import org.globus.myproxy.InitParams;
import org.globus.myproxy.MyProxy;
import org.globus.myproxy.MyProxyException;
import org.ietf.jgss.GSSCredential;
import org.ietf.jgss.GSSException;

import edu.utexas.tacc.wcs.mpg.Settings;
import edu.utexas.tacc.wcs.mpg.common.auth.client.myproxy.MyProxyLogon;
import edu.utexas.tacc.wcs.mpg.exceptions.MissingCredentialException;

/**
 * Client to interact with a standard MyProxy server set up as an online CA.
 * 
 * @author dooley
 *
 */
public class DefaultMyProxyClient extends AbstractMyProxyClient implements MyProxyClient 
{
	private static final Logger logger = Logger.getLogger(DefaultMyProxyClient.class);
	
	private MyProxyLogon myproxy = null;
	private String host;
	private int port;
	private String username = null;;
	private String passphrase = null;
	
	/**
	 * Default constructor uses service settings to configure connection since
	 * this is a single service instance.
	 */
	public DefaultMyProxyClient(String host, int port, String username, String passphrase)
	{
		this.username = username;
		this.passphrase = passphrase;
		
		setHost(host);
		setPort(port);
		
		this.myproxy = new MyProxyLogon(host, port);
		
		myproxy.requestTrustRoots(true);
		myproxy.setUsername(username);
		myproxy.setPassphrase(passphrase);
		
		try {
			bootstrapTrust(true);
		} catch (Exception e) {
			logger.error("Failed to bootstrap trust with myproxy server", e);
		}
	}
	
	@Override
	public String getHost() {
		return host;
	}

	@Override
	public void setHost(String host) {
		this.host = host;
	}

	@Override
	public int getPort() {
		return port;
	}

	@Override
	public void setPort(int port) {
		this.port = port;
	}
	
	/* (non-Javadoc)
	 * @see edu.utexas.tacc.wcs.oa4mp.common.auth.client.MyProxyClient#getTrustroots()
	 */
    @Override
	public Map<String, String> getTrustroots() throws MyProxyException 
    {
    	try {
			myproxy.logon();
		} catch (Exception e) {
			throw new MyProxyException("Failed to authenticate to the myproxy server " + host, e);	
		}
    	finally {
    		try {myproxy.disconnect();} catch (Exception e){}
    	}
	
		return myproxy.writeTrustRootsAsMap();
    }

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.wcs.oa4mp.common.auth.client.MyProxyClient#getCredentialForUser(java.lang.String, java.lang.String, int, java.util.List, java.util.List)
	 */
	@Override
	public GSSCredential getCredentialForUser(String username, 
											  String credentialName, 
											  int lifetime, 
											  List<String> vomses, 
											  List<String> voname) throws MyProxyException 
	{
		
		MyProxy myproxyClient = new MyProxy(host, port);
			
		GetParams request = new GetParams();
		request.setUserName(this.username);
		request.setPassphrase(this.passphrase);
		request.setCredentialName(credentialName);
		request.setLifetime(lifetime);
		request.setVomses(vomses);
		request.setVoname(voname);
		
		return myproxyClient.get(null, request);
	}
	
	/* (non-Javadoc)
	 * @see edu.utexas.tacc.wcs.oa4mp.common.auth.client.MyProxyClient#getAllUserCredentails(java.lang.String)
	 */
	@Override
	public CredentialInfo[] getAllUserCredentails(String username) throws MyProxyException
	{
		try 
		{
			GSSCredential cred = getCredentialForUser(username, null, Settings.DEFAULT_CREDENTIAL_LIFETIME, null, null);
			
			MyProxy myproxyClient = new MyProxy(host, port);
			InfoParams request = new InfoParams();
			request.setUserName(username);
			
			return myproxyClient.info(cred, request);
		}
		catch (MyProxyException e) 
		{
			if (e.getCause() != null && e.getCause().getMessage().contains("no credentials found")) {
				return new CredentialInfo[]{};
			} else {
				throw e;
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.utexas.tacc.wcs.oa4mp.common.auth.client.MyProxyClient#storeCredentialForUser(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public GSSCredential storeCredentialForUser(String username,
											  String delegatedUsername, 
											  String delegatedCredentialPassword,
											  String delegatedCredentialName, 
											  int delegatedLifetime,
											  String delegatedCredentialDescription,
											  String delgatedRenewer,
											  String delegatedRetriever,
											  String trustedRetriever) throws MyProxyException, GSSException 
	{
//		throw new MyProxyException("PUT functionality is not supported, nor requrired when using OA4MP. "
//				+ "A valid credential will be issued to anyone you explicitly grant access to through "
//				+ "the OAuth server.");
		
		GSSCredential cred = getCredentialForUser(username, null, delegatedLifetime, null, null);
		System.out.println("Storing credential with " + cred.getRemainingLifetime() + " seconds remaining.");
		MyProxy myproxyClient = new MyProxy(host, port);
		InitParams initParams = new InitParams();
		initParams.setCredentialDescription(delegatedCredentialDescription);
		initParams.setCredentialName(delegatedCredentialName);
		initParams.setLifetime(cred.getRemainingLifetime());
		initParams.setPassphrase(delegatedCredentialPassword);
		initParams.setRenewer(delgatedRenewer);
		initParams.setRetriever(delegatedRetriever);
		initParams.setTrustedRetriever(trustedRetriever);
		initParams.setUserName(delegatedUsername);
		
		myproxyClient.put(cred, initParams);
		
		return cred;
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.wcs.oa4mp.common.auth.client.MyProxyClient#deleteCredential(java.lang.String, java.lang.String)
	 */
	@Override
	public void deleteCredential(String username, String credentialName) 
	throws MissingCredentialException, MyProxyException
	{
//		throw new MyProxyException("DELETE functionality is not supported, nor requrired when using OA4MP. "
//				+ "The short-term credentials issued to application you delegated permission to will expire on their own. "
//				+ "To prevent previously delegated applications from obtaining credentials in the future, "
//				+ "you may revoke their access via the OAuth server.");
		
		GSSCredential cred;
		try {
			cred = getCredentialForUser(username, credentialName, Settings.DEFAULT_CREDENTIAL_LIFETIME, null, null);
		} catch (MyProxyException e) {
			throw new MissingCredentialException("Failed to retrieve credential for user " + username, e);
		}
		
		MyProxy myproxyClient = new MyProxy(host, port);
		DestroyParams destroyParams = new DestroyParams();
		destroyParams.setCredentialName(credentialName);
		destroyParams.setUserName(username);
		myproxyClient.destroy(cred, destroyParams);
	}
}
