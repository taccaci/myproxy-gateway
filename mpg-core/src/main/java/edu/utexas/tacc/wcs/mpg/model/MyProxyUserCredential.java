/**
 * Copyright (c) 2014, Texas Advanced Computing Center
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the University of Texas at Austin nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */
package edu.utexas.tacc.wcs.mpg.model;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import edu.utexas.tacc.wcs.mpg.Settings;
import edu.utexas.tacc.wcs.mpg.common.persistence.TenancyHelper;
import edu.utexas.tacc.wcs.mpg.exceptions.NotificationException;

@Entity
@Table(name = "myproxy_user_credentials")
@FilterDef(name="myproxyTenantFilter", parameters=@ParamDef(name="tenantId", type="string"))
@Filters(@Filter(name="myproxyTenantFilter", condition="tenant_id=:tenantId"))
public class MyProxyUserCredential 
{	
	private Long id;					// private db id
	private String uuid;				// uuid of this trigger
	private String owner;				// uuid of associated Object
	private String credentialName;		// uuid of associated Object 
	private String credentialDescription;
	private String credential;
	private String renewers;
	private String retrievers;
	private Integer requestedLifetime;		// status event that needs met to make the call
	private String ipAddress;			// url to post back to
	private String tenantId;			// tenant to which this entity belongs
	private Date renewedAt;				// last time an attempt was sent
	private Date createdAt = new Date();	// when was the notification created
	
	public MyProxyUserCredential() {
		this.uuid = UUID.randomUUID().toString();
		this.tenantId = TenancyHelper.getCurrentTenantId();
	}
	
	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue
	@Column(name = "`id`", unique = true, nullable = false)
	public Long getId()
	{
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id)
	{
		this.id = id;
	}	
	
	/**
	 * @return the uuid
	 */
	@Column(name = "uuid", nullable = false, length = 64, unique = true)
	public String getUuid()
	{
		return uuid;
	}

	/**
	 * @param nonce the uuid to set
	 */
	public void setUuid(String uuid) throws NotificationException
	{
		if (StringUtils.length(uuid) > 64) {
			throw new NotificationException("Invalid notification uuid. " +
					"Notification associatedUuid must be less than 64 characters.");
		}
		this.uuid = uuid;
	}
	
	/**
	 * @return the owner
	 */
	@Column(name = "owner", nullable = false, length = 256)
	public String getOwner()
	{
		return owner;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) throws NotificationException
	{
		if (StringUtils.length(owner) > 256) {
			throw new NotificationException("Invalid notification owner. " +
					"Notification owner must be less than 32 characters.");
		}
		this.owner = owner;
	}

	/**
	 * @return the credentialName
	 */
	@Column(name = "name", nullable = false, length = 64)
	public String getCredentialName()
	{
		return credentialName;
	}

	/**
	 * @param nonce the credentialName to set
	 */
	public void setCredentialName(String credentialName) throws NotificationException
	{
		if (StringUtils.length(credentialName) > 64) {
			throw new NotificationException("Invalid credname. " +
					"credname must be less than 64 characters.");
		}
		this.credentialName = credentialName;
	}

	@Column(name = "description", nullable = false, length = 256)
	public String getCredentialDescription() {
		return credentialDescription;
	}

	public void setCredentialDescription(String credentialDescription) throws NotificationException {
		if (StringUtils.length(credentialName) > 256) {
			throw new NotificationException("Invalid description. " +
					"description must be less than 256 characters.");
		}
		this.credentialDescription = credentialDescription;
	}

	@Column(name = "renewers", nullable = false, length = 256)
	public String getRenewers() {
		return renewers;
	}

	/**
	 * @param renewers
	 * @throws NotificationException 
	 */
	public void setRenewers(String renewers) throws NotificationException {
		if (StringUtils.length(credentialName) > 256) {
			throw new NotificationException("Invalid renewers. " +
					"renewers must be less than 256 characters.");
		}
		this.renewers = renewers;
	}

	/**
	 * @return
	 */
	@Column(name = "retrievers", nullable = false, length = 256)
	public String getRetrievers() {
		return retrievers;
	}

	/**
	 * @param retrievers
	 * @throws NotificationException 
	 */
	public void setRetrievers(String retrievers) throws NotificationException {
		if (StringUtils.length(credentialName) > 256) {
			throw new NotificationException("Invalid retrievers. " +
					"retrievers must be less than 256 characters.");
		}
		this.retrievers = retrievers;
	}

	/**
	 * @return the credential
	 */
	@Column(name = "`credential`", nullable = false, length = 8192)
	public String getCredential()
	{
		return credential;
	}

	/**
	 * @param credential the credential to set
	 */
	public void setCredential(String credential) throws NotificationException
	{
		if (StringUtils.isEmpty(credential)) {
			throw new NotificationException("Invalid credential. " +
					"credential cannot be null.");
		}
		if (StringUtils.length(credential) > 8192) {
			throw new NotificationException("Invalid credential. " +
					"credential must be less than 8192 characters.");
		}
		this.credential = credential;
	}

	/**
	 * @return the ipAddress
	 */
	@Column(name = "ip_address", nullable=false, length = 32)
	public String getIpAddress()
	{
		return ipAddress;
	}

	/**
	 * @param ipAddress the ipAddress to set
	 * @throws NotificationException 
	 */
	public void setIpAddress(String ipAddress) throws NotificationException
	{
		if (StringUtils.isEmpty(ipAddress)) {
			throw new NotificationException("Invalid ipAddress. " +
					"ipAddress cannot be null.");
		}
		if (StringUtils.length(ipAddress) > 32) {
			throw new NotificationException("Invalid ipAddressl. " +
					"ipAddress must be less than 32 characters.");
		}
		
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the response
	 */
	@Column(name = "requested_lifetime", nullable = false)
	public Integer getRequestedLifetime()
	{
		return requestedLifetime;
	}

	/**
	 * @param RequestedLifetime the RequestedLifetime to set
	 */
	public void setRequestedLifetime(Integer requestedLifetime)
	{
		this.requestedLifetime = requestedLifetime;
	}

	/**
	 * @return the tenantId
	 */
	@Column(name = "tenant_id", nullable=false, length = 128)
	public String getTenantId()
	{
		return tenantId;
	}

	/**
	 * @param tenantId the tenantId to set
	 */
	public void setTenantId(String tenantId)
	{
		this.tenantId = tenantId;
	}

	/**
	 * @return the last time this credential was renewed
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "renewed_at", nullable = false, length = 19)
	public Date getRenewedAt()
	{
		return renewedAt;
	}

	/**
	 * @param renewedAt the renewedAt to set
	 */
	public void setRenewedAt(Date refreshedAt)
	{
		this.renewedAt = refreshedAt;
	}
	
	/**
	 * @return the creation time
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", nullable = false, length = 19)
	public Date getCreatedAt()
	{
		return createdAt;
	}
	
	@Transient
	public Date getExpirationTime()
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(getRenewedAt() == null ? getCreatedAt() : getRenewedAt());
		cal.add(Calendar.SECOND, getRequestedLifetime());
		return cal.getTime();
	}

	/**
	 * @param created the created to set
	 */
	public void setCreatedAt(Date createdAt)
	{
		this.createdAt = createdAt;
	}
	
	/**
	 * Serializes the user credential into a JSON object
	 * @return json representation of this object.
	 * @throws NotificationException 
	 */
	public String toJSON() throws NotificationException
	{
		ObjectMapper mapper = new ObjectMapper();
		try
		{	
			ObjectNode json = mapper.createObjectNode()
				.put("owner", getOwner())
				.put("credname", getCredentialName())
				.put("creddesc", getCredentialDescription())
				.put("credential", getCredential())
				.put("lifetime", getRequestedLifetime())
				.put("renewable_by", getRenewers())
				.put("retrivable_by", getRetrievers())
				.put("expires_at", new DateTime(getExpirationTime()).toString())
				.put("created_at", new DateTime(getCreatedAt()).toString());
			if (getRenewedAt() == null) {
				json.putNull("renewed_at");
			} else {
				json.put("renewed_at", new DateTime().toString());
			}
			JsonNode href = mapper.createObjectNode().put("href", Settings.SERVICE_BASE_URL + "credentials/" + (StringUtils.isEmpty(getCredentialName()) ? "" : getCredentialName()) );
			JsonNode self = mapper.createObjectNode().putPOJO("self", href);
			json.putPOJO("_links",self);
			return json.toString();
		}
		catch (Exception e)
		{
			throw new NotificationException("Failed to serialize credential", e);
		}		
	}
	
	public String toString() {
		return getOwner() + ": " + getCredentialName() + " " + new DateTime(createdAt.getTime() + requestedLifetime.longValue()).toString();
	}	
}
