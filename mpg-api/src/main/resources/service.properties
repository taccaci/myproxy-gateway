# OAuth 2.0 for MyProxy service properties

################################################################################
# Service debug parameters
################################################################################

mpg.debug.mode=${mpg.debug.mode}
mpg.debug.username=${mpg.debug.username}

# This is the version number of the API service returned in every JSON response
mpg.api.version=${mpg.api.version}

# The base url of the service used in the hypermedia portion of each response.
mpg.base.url=${mpg.base.url}

# These are the ports the service will run on if you run it as a standalone jetty app
mpg.jetty.port=${mpg.jetty.port}
mpg.jetty.ajp.port=${mpg.jetty.ajp.port}

################################################################################
# 	Service auth settings
################################################################################

# Specifies the identity provider used during authentication with the service.
# Valid values are listed below. Defaults to NONE.
# 1. NONE - HTTP BASIC auth will be preformed and no validation will be done.
# 2. LDAP - HTTP BASIC auth will be preformed and the service will attempt to 
# 	 bind to the ldap server configured in the mpg.auth.ldap.url and 
# 	 mpg.auth.ldap.base.dn settings. 
# 3. MYPROXY - HTTP BASIC auth will be performed and the service will perform 
# 	 a GET on the myproxy server configured in the mpg.auth.myproxy.host and 
#	 mpg.auth.myproxy.port settings. If no myproxy server is specified there,
#	 the delegated myproxy server configured in the mpg.delegated.myproxy.host 
# 	 and mpg.delegated.myproxy.port settings will be used. 
# 4. WSO2 - The service is protected behind a WSO2 Identity Server or API Manager
#    and will recieve a JWT header in the request.
mpg.auth.type=${mpg.auth.type}

################################################################################
# 	LDAP authentication 
################################################################################

# These are the ldap connection parameters used when mpg.auth.type = ldap. 
# The service will attempt to do a binding connection using the username and 
# password provided during HTTP BASIC auth.
mpg.auth.ldap.url=${mpg.auth.ldap.url}
mpg.auth.ldap.base.dn=${mpg.auth.ldap.base.dn}

# Location of the keystore and trustore used for SSL authentication
mpg.auth.ldap.keystore=${mpg.auth.ldap.keystore}
mpg.auth.ldap.truststore=${mpg.auth.ldap.truststore}

# This is the name of the header containing the JWT expected by the service 
# when mpg.auth.type = wso2. Defaults to x-jwt-assertion.
mpg.auth.wsow.jwt.header=${mpg.auth.wsow.jwt.header}

# This is the connection properties for the myproxy server used to authenticate
# the user when mpg.auth.type = myproxy. This can be different than the 
# myproxy server from which the delegated credential will be obtained. This 
# can be useful when using the service as an API facade. If left blank, the
# mpg.myproxy.host and mpg.myproxy.port settings will be used.
mpg.auth.myproxy.host=${mpg.auth.myproxy.host}
mpg.auth.myproxy.port=${mpg.auth.myproxy.port}

################################################################################
# 	MyProxy configs
################################################################################

# Specifies how the service will be retrieving a credential from the delegated 
# MyProxy server. Valid values are given below. The default value is PROXY.
# 1. PROXY - The user credentials used to authenticate to the service will be used
#	 to retrieve a credential from the delegated MyProxy service. This is not a 
#	 valid value when mpg.auth.type = wso2 as the service does not recieve 
# 	 any user credentials in the JWT header.
# 2. IMPLICIT - The service will use an existing trust relationship set up with
#	 the delegated MyProxy server to act on behalf of the user. This requires 
# 	 the service to have a valid X.509 certificate and key to communicate with
#	 the delegated MyProxy server.
mpg.delegated.myproxy.mode=${mpg.delegated.myproxy.mode}

mpg.delegated.myproxy.host=${mpg.delegated.myproxy.host}
mpg.delegated.myproxy.port=${mpg.delegated.myproxy.port}

# These are the path to your hostcert.pem and hostkey.pem files. If not given,
# the usual locations on the server are checked.
# 1. If X509_USER_CERT and X509_USER_CERT exist and contain a valid certificate and key, those files are used.
# 2. Otherwise, if the files /etc/grid-security/hostcert.pem and /etc/grid-security/hostkey.pem exist and contain a valid certificate and key, those files are used.
# 3. Otherwise, if the files $GLOBUS_LOCATION/etc/grid-security/hostcert.pem and $GLOBUS_LOCATION/etc/grid-security/hostkey.pem exist and contain a valid certificate and key, those files are used.
# 4. Otherwise, if the files hostcert.pem and hostkey.pem in the user's .globus directory, exist and contain a valid certificate and key, those files are used.
mpg.delegated.myproxy.hostcert=${mpg.delegated.myproxy.hostcert}
mpg.delegated.myproxy.hostkey=${mpg.delegated.myproxy.hostkey}

# This is the password for your hostkey. It is needed if your hostkey has a
# password assigned to it.
mpg.delegated.myproxy.hostkey.pass=${mpg.delegated.myproxy.hostkey.pass}

################################################################################
# Default Credential Settings
################################################################################

# Default lifetime in seconds for which a credential should be valid. Defaults 
# to 43200
mpg.default.lifetime=${mpg.default.lifetime}

################################################################################
# Mail server settings
################################################################################

# Settings for the mail server used to alert the service administrators when 
# crazy things happen.
mail.smtps.auth=${mpg.smtp.auth}
mail.smtps.user=${mpg.smtp.user}
mail.smtps.passwd=${mpg.smtp.password}
mail.smtps.host=${mpg.smtp.host}
mail.smtps.from.name=${mpg.smtp.from.name}
mail.smtps.from.address=${mpg.smtp.from.address}

