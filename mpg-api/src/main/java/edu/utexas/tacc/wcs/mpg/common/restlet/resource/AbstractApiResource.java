/**
 * Copyright (c) 2014, Texas Advanced Computing Center
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the University of Texas at Austin nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */
package edu.utexas.tacc.wcs.mpg.common.restlet.resource;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

import edu.utexas.tacc.wcs.mpg.Settings;
import edu.utexas.tacc.wcs.mpg.common.auth.client.AuthenticationType;
import edu.utexas.tacc.wcs.mpg.common.exceptions.PermissionException;
import edu.utexas.tacc.wcs.mpg.common.persistence.TenancyHelper;

/**
 * @author dooley
 *
 */
public class AbstractApiResource 
{
	private static final Logger log = Logger.getLogger(AbstractApiResource.class);
	
	@Context
	protected UriInfo uriInfo;
	
	@Context
	protected Request request;
	
	@Context 
	protected HttpHeaders headers;

	@Context
	protected SecurityContext securityContext;
	
	/**
	 * Should the response be pretty printed.
	 * @return
	 */
	public boolean isPrettyPrint() {
		String pretty = getUriInfo().getQueryParameters().getFirst("pretty");
		return Boolean.parseBoolean(pretty);
	}
	
	/**
	 * Returns the max number of items to return.
	 * @return 
	 */
	public int getLimit() {
		String count = getUriInfo().getQueryParameters().getFirst("limit");
		if (NumberUtils.isDigits(count)) {
			return Integer.parseInt(count);
		} else {
			return 100;
		}
	}
	
	/**
	 * Returns the number of items to offset the response set.
	 * @return
	 */
	public int getOffset() {
		String offset = getUriInfo().getQueryParameters().getFirst("offset");
		return NumberUtils.toInt(offset);
	}
	
	public String getAuthenticatedUsername() 
	{	
		if (Settings.DEBUG) {
			return Settings.DEBUG_USERNAME;
		} else if (StringUtils.equalsIgnoreCase(AuthenticationType.WSO2.name(), Settings.AUTH_TYPE)){
			return TenancyHelper.getCurrentEndUser();
		} else {
			if (this.getSecurityContext() != null) {
				if (this.getSecurityContext().getUserPrincipal() != null) {
					return this.getSecurityContext().getUserPrincipal().getName();
				}
			}
			
			log.warn("Unknown user accessing the service");
			return null;
		}
	}
	
	protected void checkClientPrivileges(String username) 
	{
		if (StringUtils.isEmpty(username)) {
			throw new WebApplicationException(
					new PermissionException("No client id provided."),
					Status.NOT_FOUND);
		}
		else if (!StringUtils.equals(getAuthenticatedUsername(), username)) {
			if (!this.getSecurityContext().isUserInRole("admin")) {
				throw new WebApplicationException(
						new PermissionException("Client does not have permission to view the requested resource."), 
						Status.UNAUTHORIZED);
			}			 
		}
	}

	/**
	 * @return the uriInfo
	 */
	public UriInfo getUriInfo()
	{
		return uriInfo;
	}

	/**
	 * @param uriInfo the uriInfo to set
	 */
	public void setUriInfo(UriInfo uriInfo)
	{
		this.uriInfo = uriInfo;
	}

	/**
	 * @return the securityContext
	 */
	public SecurityContext getSecurityContext()
	{
		return securityContext;
	}

	/**
	 * @param securityContext the securityContext to set
	 */
	public void setSecurityContext(SecurityContext securityContext)
	{
		this.securityContext = securityContext;
	}
}