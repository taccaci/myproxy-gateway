/**
 * Copyright (c) 2014, Texas Advanced Computing Center
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the University of Texas at Austin nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */
package edu.utexas.tacc.wcs.mpg.service.resources.impl;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.globus.myproxy.CredentialInfo;
import org.ietf.jgss.GSSCredential;
import org.restlet.Request;
import org.restlet.data.Status;
import org.restlet.resource.ResourceException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import edu.utexas.tacc.wcs.mpg.Settings;
import edu.utexas.tacc.wcs.mpg.common.auth.client.MyProxyClient;
import edu.utexas.tacc.wcs.mpg.common.auth.client.MyProxyClientFactory;
import edu.utexas.tacc.wcs.mpg.common.persistence.TenancyHelper;
import edu.utexas.tacc.wcs.mpg.common.representation.ApiSuccessRepresentation;
import edu.utexas.tacc.wcs.mpg.common.restlet.resource.AbstractApiResource;
import edu.utexas.tacc.wcs.mpg.dao.MyProxyUserCredentialDAO;
import edu.utexas.tacc.wcs.mpg.dao.MyProxyUserCredentialDAOImpl;
import edu.utexas.tacc.wcs.mpg.exceptions.MissingCredentialException;
import edu.utexas.tacc.wcs.mpg.managers.MyProxyUserCredentialPermissionManager;
import edu.utexas.tacc.wcs.mpg.model.MyProxyUserCredential;
import edu.utexas.tacc.wcs.mpg.service.resources.MyProxyResource;

/**
 * Main service bean for the oa4mp service. Nearly all functionality is delegated to the
 * MyProxyClient class and core libraries.
 * 
 * @author dooley
 */
@Path("")
@Produces("application/json")
public class MyProxyResourceImpl extends AbstractApiResource implements MyProxyResource
{
	private static final Logger log = Logger.getLogger(MyProxyResourceImpl.class);
	
	protected MyProxyUserCredentialDAO dao = new MyProxyUserCredentialDAOImpl();
	protected  MyProxyUserCredentialPermissionManager pm = null;
//	private static MyProxy myproxy = new MyProxy(Settings.DELEGATED_MYPROXY_HOST, Settings.DELEGATED_MYPROXY_PORT);
//	private static GSSCredential serviceProxy = null;
//	
	public MyProxyResourceImpl() {
//		try {
//			myproxy.bootstrapTrust();
//			serviceProxy = myproxy.get(Settings.MYPROXY_TRUSTED_USERNAME, Settings.MYPROXY_TRUSTED_PASSWORD, Settings.DEFAULT_CREDENTIAL_LIFETIME);
//		} catch (MyProxyException e) {
//			throw new RuntimeException("Failed to initialize communication with myproxy server " + Settings.MYPROXY_HOST, e);
//		}
		
	}
	
	/**
	 * Returns a MyProxyClient based on the type of authentication and delegation
	 * configured in the service.
	 * 
	 * @return MyProxyClient
	 */
	private MyProxyClient getMyProxyClientForSession()
	{
		return MyProxyClientFactory.getInstance(Settings.AUTH_TYPE, 
												getSessionUsername(), 
												getSessionPassphrase());
	}
	
	/* (non-Javadoc)
	 * @see edu.utexas.tacc.wcs.oa4mp.service.resources.MyProxyResource#retrieveTrustroots()
	 */
	@GET
	@Path("trustroots")
	@Override
	public Response retrieveTrustroots()
	{
		ObjectMapper mapper = new ObjectMapper();
		try 
		{	
			MyProxyClient myproxy = getMyProxyClientForSession();
			
			Map<String,String> trustRootMap = myproxy.getTrustroots();
			
			ArrayNode arrayNode = mapper.createArrayNode();
			for (String filename: trustRootMap.keySet()) 
			{
				arrayNode.addObject().put("filename", filename).put("content", trustRootMap.get(filename));
			}
			return Response.ok(new ApiSuccessRepresentation(arrayNode.toString(), isPrettyPrint())).build();
		} 
		catch (Throwable e) 
		{
			throw new ResourceException(Status.SERVER_ERROR_INTERNAL, e.getMessage(), e);
		}	
	}									
	
	/* (non-Javadoc)
	 * @see edu.utexas.tacc.wcs.oa4mp.service.resources.MyProxyResource#getNewCredential(java.lang.String, java.lang.Boolean)
	 */
	@GET
	@Path("credentials")
	@Override
	public Response getCredentialList(@QueryParam("username") String username,
									 @QueryParam("trustroots") Boolean returnTrustRoots)
	{
		log.info("[" + getAuthenticatedUsername() + "] GET credential list for " + username);
		
		if (StringUtils.isEmpty(username)) {
			username = getAuthenticatedUsername();
		}
		try 
		{	
			MyProxyClient myproxy = getMyProxyClientForSession();
			
			CredentialInfo[] credInfos = myproxy.getAllUserCredentails(username);
			
			String[] userCredentials = new String[credInfos.length];
			
			for (int i=getOffset(); i< Math.min((getLimit()+getOffset()), credInfos.length); i++)
			{
				MyProxyUserCredential mpuc = new MyProxyUserCredential();
				
				mpuc.setOwner(getAuthenticatedUsername());
				mpuc.setCredentialName(credInfos[i].getName());
				mpuc.setRenewers(credInfos[i].getRenewers());
				mpuc.setRenewers(credInfos[i].getRetrievers());
				mpuc.setOwner(credInfos[i].getOwner());
				mpuc.setCredentialDescription(credInfos[i].getDescription());
				mpuc.setCreatedAt(credInfos[i].getStartTimeAsDate());
				mpuc.setRequestedLifetime((int)((credInfos[i].getEndTime() - credInfos[i].getStartTime())/1000));
				mpuc.setIpAddress(Request.getCurrent().getClientInfo().getAddress());
				
				userCredentials[i] = mpuc.toJSON();
			}	
			
			return Response.ok(new ApiSuccessRepresentation("[" +  StringUtils.join(userCredentials, ",") + "]", isPrettyPrint())).build();
		} 
		catch (Throwable e) 
		{
			log.error(e);
			throw new ResourceException(Status.SERVER_ERROR_INTERNAL, e.getMessage(), e);
		}	
	}
	
	/* (non-Javadoc)
	 * @see edu.utexas.tacc.wcs.oa4mp.service.resources.MyProxyResource#delegateNewCredential(java.lang.String, java.lang.String, java.lang.String, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.Boolean, java.lang.String, java.lang.String, java.lang.Boolean)
	 */
	@POST
	@Path("credentials")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Override
	public Response retrieveNewCredential(@FormParam("proxy_lifetime") @DefaultValue("")String lifetime,
										 @QueryParam("trustroots") Boolean returnTrustRoots)
	{
		Integer proxyLifetime = NumberUtils.toInt(lifetime, Settings.DEFAULT_CREDENTIAL_LIFETIME);
		
		log.info("[" + getAuthenticatedUsername() + "] POST new credential valid for " + proxyLifetime + " seconds");
		
		MyProxyUserCredential mpuc = new MyProxyUserCredential();
		String response = null;
		try 
		{	
			MyProxyClient myproxy = getMyProxyClientForSession();
			
			GSSCredential cred = myproxy.getCredentialForUser(getAuthenticatedUsername(), null, proxyLifetime, null, null);
			
			String serializedCredential = myproxy.serializeCredential(cred);
			
			mpuc.setOwner(getAuthenticatedUsername());
			mpuc.setCredentialName(null);
			mpuc.setIpAddress(Request.getCurrent().getClientInfo().getAddress());
			mpuc.setCredential(serializedCredential);
			mpuc.setRequestedLifetime(cred.getRemainingLifetime());
			
			if (returnTrustRoots) 
			{
				ObjectMapper mapper = new ObjectMapper();
				ObjectNode json = (ObjectNode)mapper.readTree(mpuc.toJSON());
				
				Map<String,String> trustRootMap = myproxy.getTrustroots();
				
				ArrayNode arrayNode = mapper.createArrayNode();
				for (String filename: trustRootMap.keySet()) 
				{
					arrayNode.addObject().put("filename", filename).put("content", trustRootMap.get(filename));
				}
				json.putArray("trustroots").addAll(arrayNode);
				response = json.toString();
			}
			else
			{
				response = mpuc.toJSON();
			}
			
			return Response.ok(new ApiSuccessRepresentation(response, isPrettyPrint())).build();
		} 
		catch (Throwable e) 
		{
			log.error(e);
			throw new ResourceException(Status.SERVER_ERROR_INTERNAL, e.getMessage(), e);
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.utexas.tacc.wcs.oa4mp.service.resources.MyProxyResource#getCredentialByName(java.lang.String, java.lang.String, java.lang.String, java.lang.Boolean)
	 */
	@GET
	@Path("credentials/{credname}")
	@Override
	public Response getCredentialByName(@PathParam("credname") String credentialName,
										@QueryParam("dn_as_username") String useDnAsUsername,
										@QueryParam("proxy_lifetime") String lifetime,
										@QueryParam("trustroots") Boolean returnTrustRoots)
	{
		Integer proxyLifetime = NumberUtils.toInt(lifetime, Settings.DEFAULT_CREDENTIAL_LIFETIME);
		
		log.info("[" + getAuthenticatedUsername() + "] GET delegated credential " + credentialName + " valid for " + proxyLifetime + " seconds");
		
		if (StringUtils.isEmpty(credentialName)) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "No name specified for delegated credential.");
		}
		
		
		MyProxyUserCredential mpuc = new MyProxyUserCredential();
		MyProxyClient myproxy = getMyProxyClientForSession();
		String response = null;
		try 
		{
			GSSCredential cred = myproxy.getCredentialForUser(getAuthenticatedUsername(), credentialName, proxyLifetime, null, null);
			
			mpuc.setOwner(getAuthenticatedUsername());
			mpuc.setCredentialName(credentialName);
			mpuc.setIpAddress(Request.getCurrent().getClientInfo().getAddress());
			mpuc.setCredential(myproxy.serializeCredential(cred));
			mpuc.setRequestedLifetime(cred.getRemainingLifetime());
			
			if (returnTrustRoots) 
			{
				ObjectMapper mapper = new ObjectMapper();
				ObjectNode json = (ObjectNode)mapper.readTree(mpuc.toJSON());
				
				Map<String,String> trustRootMap = myproxy.getTrustroots();
				
				ArrayNode arrayNode = mapper.createArrayNode();
				for (String filename: trustRootMap.keySet()) 
				{
					arrayNode.addObject().put("filename", filename).put("content", trustRootMap.get(filename));
				}
				json.putArray("trustroots").addAll(arrayNode);
				response = json.toString();
			}
			else
			{
				response = mpuc.toJSON();
			}
			return Response.ok(new ApiSuccessRepresentation(response, isPrettyPrint())).build();
		} 
		catch (Throwable e) 
		{
			log.error(e);
			throw new ResourceException(Status.SERVER_ERROR_INTERNAL, e.getMessage(), e);
		}	
	}
	
	/* (non-Javadoc)
	 * @see edu.utexas.tacc.wcs.oa4mp.service.resources.MyProxyResource#updateCredentialByName(java.lang.String, java.lang.String, java.lang.String, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.Boolean, java.lang.String, java.lang.String, java.lang.Boolean)
	 */
	@PUT
	@Path("credentials/{credname}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Override
	public Response updateCredentialByName(@PathParam("credname") String credentialName,
										 @FormParam("creddesc") String credentialDescription,
										 @FormParam("proxy_lifetime") String lifetime,
										 @FormParam("allow_anonymous_retrievers") Boolean allowAnonymousRetrievers,
										 @FormParam("allow_anonymous_renewers") Boolean allowAnonymousRenewers,
										 @FormParam("regex_dn_match") Boolean regexDnMatch,
										 @FormParam("match_cn_only") Boolean matchCnOnly,
										 @FormParam("retrievable_by") String retrievableBy,
										 @FormParam("renewable_by") String renewableBy,
										 @FormParam("retrievable_by_cert") String retrievableByCert,
										 @FormParam("retrieve_key") String retrieveKey,
										 @FormParam("dn_as_username") Boolean useDnAsUsername,
										 @FormParam("stored_username") String storedUsername,
										 @FormParam("stored_password") String storedPassword,
										 @QueryParam("trustroots") Boolean returnTrustRoots)
	{
		Integer proxyLifetime = NumberUtils.toInt(lifetime, Settings.DEFAULT_CREDENTIAL_LIFETIME);
		
		log.info("[" + getAuthenticatedUsername() + "] PUT credential " + credentialName + " to " + storedUsername + " for " + proxyLifetime + " seconds");
		
		if (StringUtils.isEmpty(storedPassword)) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "No password specified for delegated credential.");
		}
		
		MyProxyUserCredential mpuc = new MyProxyUserCredential();
		String response = null;
		try 
		{	
			MyProxyClient myproxy = getMyProxyClientForSession();
			
			GSSCredential cred = myproxy.storeCredentialForUser(getAuthenticatedUsername(),
					  (useDnAsUsername ? null : storedUsername), 
					  storedPassword,
					  credentialName, 
					  proxyLifetime,
					  credentialDescription,
					  renewableBy,
					  retrievableBy,
					  retrievableByCert);
			
			String serializedCredential = myproxy.serializeCredential(cred);
			
			mpuc.setOwner(getAuthenticatedUsername());
			mpuc.setCredentialName(credentialName);
			mpuc.setIpAddress(Request.getCurrent().getClientInfo().getAddress());
			mpuc.setCredential(serializedCredential);
			mpuc.setRequestedLifetime(cred.getRemainingLifetime());
			
			if (returnTrustRoots) 
			{
				ObjectMapper mapper = new ObjectMapper();
				ObjectNode json = (ObjectNode)mapper.readTree(mpuc.toJSON());
				
				Map<String,String> trustRootMap = myproxy.getTrustroots();
				
				ArrayNode arrayNode = mapper.createArrayNode();
				for (String filename: trustRootMap.keySet()) 
				{
					arrayNode.addObject().put("filename", filename).put("content", trustRootMap.get(filename));
				}
				json.putArray("trustroots").addAll(arrayNode);
				response = json.toString();
			}
			else
			{
				response = mpuc.toJSON();
			}
			
			return Response.ok(new ApiSuccessRepresentation(response, isPrettyPrint())).build();
		} 
		catch (Throwable e) 
		{
			log.error(e);
			throw new ResourceException(Status.SERVER_ERROR_INTERNAL, e.getMessage(), e);
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.utexas.tacc.wcs.oa4mp.service.resources.MyProxyResource#deleteCredentialByName(java.lang.String, java.lang.Boolean)
	 */
	@DELETE
	@Path("credentials/{credname}")
	@Override
	public Response deleteCredentialByName(@PathParam("credname") String credentialName,
								 	   	   @QueryParam("dn_as_username") Boolean useDnAsUsername)
	{
		log.info("[" + getAuthenticatedUsername() + "] DELETE delegated credential " + credentialName);
		
		try 
		{
			MyProxyClient myproxy = getMyProxyClientForSession();
			myproxy.deleteCredential(getAuthenticatedUsername(), credentialName);
			
			return Response.ok(new ApiSuccessRepresentation(
					"Credential " + credentialName + " deleted successfully.", null, isPrettyPrint())).build();
		} 
		catch (MissingCredentialException e) {
			throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, e.getMessage(), e);
		}
		catch (Throwable e) 
		{
			log.error(e);
			throw new ResourceException(Status.SERVER_ERROR_INTERNAL, e.getMessage(), e);
		}	
	}
	
	/**
	 * Locates the correct username to use when communicating with the remote myproxy server.
	 * If JWT auth is configured, it uses the service username from the oa4mp settings.
	 * Otherwise it uses the authenticated user's username.
	 * 
	 * @return username used to auth to the myproxy server.
	 */
	public String getSessionUsername()
	{
		if (StringUtils.equalsIgnoreCase(Settings.AUTH_TYPE, "wso2")) 
		{
			return TenancyHelper.getCurrentEndUser();
		} 
		else 
		{
			if (Request.getCurrent().getChallengeResponse() == null || 
					Request.getCurrent().getChallengeResponse().getIdentifier() == null) {
				return null;
			} else {
				return Request.getCurrent().getChallengeResponse().getIdentifier();
			}
		}
	}
	
	/**
	 * Locates the correct passphrase to use when communicating with the remote myproxy server.
	 * If JWT auth is configured, it uses the service passphrase from the oa4mp settings.
	 * Otherwise it uses the authenticated user's passphrase.
	 * 
	 * @return username used to auth to the myproxy server.
	 */
	public String getSessionPassphrase() 
	{
		if (StringUtils.equalsIgnoreCase(Settings.AUTH_TYPE, "wso2")) 
		{
			return null;
		} 
		else 
		{
			if (Request.getCurrent().getChallengeResponse() == null || 
					Request.getCurrent().getChallengeResponse().getSecret() == null) {
				return null;
			} else {
				return new String(Request.getCurrent().getChallengeResponse().getSecret());
			}
		}
	}

}
