/**
 * Copyright (c) 2014, Texas Advanced Computing Center
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the University of Texas at Austin nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */
package edu.utexas.tacc.wcs.mpg.service.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author dooley
 *
 */
/**
 * @author dooley
 *
 */
public interface MyProxyResource {

	/**
	 * Returns a json array of objects containing the trust roots for the 
	 * remote myproxy server.
	 * 
	 * @return json array of trustroot objects 
	 */
	@GET
	@Path("trustroots")
	public Response retrieveTrustroots();
										
	/**
	 * Returns a list of active credentials owned by the authenticated user and
	 * available to the given username
	 * 
	 * @param username
	 * @param trustroots
	 * @return JSON object containing the credential, metadata, and optionally a trust root array.
	 */
	@GET
	@Path("credentials")
	public Response getCredentialList(@QueryParam("username") String username,
									 @QueryParam("trustroots") Boolean trustroots);
	
	
	/**
	 * Replicates the myproxy-logon command. Pulls a fresh credential from myproxy.
	 * 
	 * @param lifetime
	 * @param returnTrustRoots
	 */
	@POST
	@Path("credentials")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response retrieveNewCredential(@FormParam("proxy_lifetime") String lifetime,
								 		  @QueryParam("trustroots") Boolean returnTrustRoots);
	
	
	/**
	 * Gets a delegated credentail stored on the remote myproxy server with the given name.
	 * Analagous to the myproxy-get-delegation command.
	 * 
	 * @param credentialName
	 * @param useDnAsUsername
	 * @param lifetime
	 * @param returnTrustRoots
	 * @return JSON object containing the credential, metadata, and optionally a trust root array.
	 */
	@GET
	@Path("credentials/{credname}")
	public Response getCredentialByName(@PathParam("credname") String credentialName,
										@QueryParam("dn_as_username") String useDnAsUsername,
										@QueryParam("proxy_lifetime") String lifetime,
										@QueryParam("trustroots") Boolean returnTrustRoots);
	
	
	/**
	 * Refreshes a stored credential by inserting a new one with the same parameters as before.
	 * 
	 * @param credentialName
	 * @param credentialDescription
	 * @param lifetime
	 * @param allowAnonymousRetrievers
	 * @param allowAnonymousRenewers
	 * @param regexDnMatch
	 * @param matchCnOnly
	 * @param retrievableBy
	 * @param renewableBy
	 * @param retrievableByCert
	 * @param retrieveKey
	 * @param useDnAsUsername
	 * @param storedUsername
	 * @param storedPassword
	 * @param returnTrustRoots
	 * @return JSON object containing the credential, metadata, and optionally a trust root array.
	 */
	@PUT
	@Path("credentials/{credname}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response updateCredentialByName(@PathParam("credname") String credentialName,
										 @FormParam("creddesc") String credentialDescription,
										 @FormParam("proxy_lifetime") String lifetime,
										 @FormParam("allow_anonymous_retrievers") Boolean allowAnonymousRetrievers,
										 @FormParam("allow_anonymous_renewers") Boolean allowAnonymousRenewers,
										 @FormParam("regex_dn_match") Boolean regexDnMatch,
										 @FormParam("match_cn_only") Boolean matchCnOnly,
										 @FormParam("retrievable_by") String retrievableBy,
										 @FormParam("renewable_by") String renewableBy,
										 @FormParam("retrievable_by_cert") String retrievableByCert,
										 @FormParam("retrieve_key") String retrieveKey,
										 @FormParam("dn_as_username") Boolean useDnAsUsername,
										 @FormParam("stored_username") String storedUsername,
										 @FormParam("stored_password") String storedPassword,
										 @QueryParam("trustroots") Boolean returnTrustRoots);
	
	
	/** 
	 * Deletes the credential with the given name from the remote myproxy server. This is
	 * analagous to the myproxy-destroy command.
	 * 
	 * @param credentialName
	 * @param useDnAsUsername
	 * @return empty response
	 */
	@DELETE
	@Path("credentials/{credname}")
	public Response deleteCredentialByName(@PathParam("credname") String credentialName,
								 	   	   @QueryParam("dn_as_username") Boolean useDnAsUsername);
}
