/**
 * Copyright (c) 2014, Texas Advanced Computing Center
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the University of Texas at Austin nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS AS IS
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */
package edu.utexas.tacc.wcs.mpg.common.auth;

import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.security.Verifier;
import org.restlet.util.Series;

import edu.utexas.tacc.wcs.mpg.Settings;
import edu.utexas.tacc.wcs.mpg.common.auth.client.JWTClient;

public class JWTVerifier implements Verifier {
	private static final Logger log = Logger.getLogger(JWTVerifier.class);
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public int verify(Request request, Response response)
	{
		Series series = (Series)request.getAttributes().get("org.restlet.http.headers");
		for (String key: (Set<String>)series.getNames()) 
		{
			if (key.toLowerCase().startsWith(Settings.LOGIN_JWT_HEADER)) 
			{
				String jwtHeader = (String)series.getFirstValue(key);
				
				log.debug(key + " : " + jwtHeader);
				
				String tenantId = "default";
				if (key.length() > StringUtils.length(Settings.LOGIN_JWT_HEADER)) {
					tenantId = key.toLowerCase().substring(Settings.LOGIN_JWT_HEADER.length() + 1);
				}
				
				return JWTClient.parse(jwtHeader, tenantId) ? Verifier.RESULT_VALID : Verifier.RESULT_INVALID;
			}
		}
		
		return Verifier.RESULT_INVALID;
	}
}
