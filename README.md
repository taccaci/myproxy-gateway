# The MyProxy Gateway Service
******

This is the Java implementation of the MyProxy Gateway (MPG) API. This service acts as a REST interface to MyProxy allowing users to:

* Retrieve a new credential
* Delegate and list stored credentials
* Retrieve a delegated credential
* Delete delegated credentials

The service supports multiple authentication mechanisms. As the name implies, the primary authentication scheme is to run the service as a protected resource behind an [OAuth 2](http://oauth.net/2/) authorization server. In this cofiguration, the API Manager will pass a [JSON Web Token](http://tools.ietf.org/html/draft-ietf-oauth-jwt-bearer-07) (JWT) to the OA4MP service with user information. The OA4MP service will then use the user information and a pre-established trust relationship with a [MyProxy](http://grid.ncsa.illinois.edu/myproxy/) server to act on behalf of the user.

Any authorization server issuing signed JWT to its protected resources will work. We recommend using the [WSO2 API Manager](http://wso2.com/products/api-manager/) or [WSO2 Identity server](http://wso2.com/products/identity-server/). Both are open source and both work with this service out of the box.

> Note: The MPG services does not store user passwords or credentials. When running the service as an OAuth 2 protected resource, the only way the service can function correctly is by first establishing a trust relationship with the MyProxy server. Without this in place, the service cannot interact with the MyProxy server on behalf of the user because the user's password is not persisted or forwarded to OA4MP service.

The MPG service can also be configured to run in standalone mode without an OAuth 2 authentication server protecting it. In this setup, the service acts as a RESTful API proxy service to a standard MyProxy service. It will use HTTP Basic authentication with either passthrough, LDAP, or MyProxy as the authenticating identity store. 

> Note: When running MPG in standalone mode, it is imporant that the MyProxy server and the MPG service are pointing at the same identity store since the same username and password will be used to authenticate to both the identity store and the MyProxy server.

## Docker support

If you are just interested in quickly testing the MPG and kicking the tires, or if you want a preconfigured, working development MyProxy server that works out of the box, we highly recommend using the included [Docker](http://docker.io) support. Docker is an open platform for developers and sysadmins to build, ship, and run distributed applications. Using Docker, you can build once and runs, scale, and replicate anywhere. 

	**NOTE: This is by far the easiest way to get started with the service and interact with it as you would in a production environment**

Inside the `myproxy/docker/standalone` folder, you will find a Dockerfile which will build and configure the MPG, test user accounts, a SSH server, a standard MyProxy CA server, and a trusted MyProxy CA server. Once it builds, you will be able to issue a simple `docker run -d agave-dev-mpg` command and run the entire stack as a simple process. There are no software stacks to manage, ports or networking to setup, or GSI security configuration to struggle with. It just works out of the box.

By running the resulting Docker container, you will be able to interact with a working version of the server running locally with a full GSI stack behind it just like you would in a production environment. For more information, view the README in the `docker` folder. 

	**For more advanced testing, you may also build and run the container in the `myproxy/docker/oauth2` container for a full stack MPG + OAuth2 setup including test accounts, client registration, interactive documentation, working services, and a sample login page.**


## Prerequisites

To build and run the MPG service you will need to have the following installed:

* Java 1.6+
* Maven 2.2+
* MySQL 5

In order to run the service in a production environment, you will need the additional prerequisites:

* Apache 2
* Tomcat 6
* 	

## Building from source

To build from source, check out the project from git and build with Maven. 

```
$!bash
$ git clone https://bitbucket.org/taccaci/myproxy-gateway.git
$ cd myproxy-gateway
$ mvn clean install
```

Once built you can run the MPG several ways.

### Embedded Jetty server

To run MPG as a standalone service using the Maven embedded [Jetty](http://www.eclipse.org/jetty/) server, run the following command:

```
#!bash
$ cd mpg-api
$ mvn -Djetty.port=8080 jetty:run-forked
```
### Executable jar

To package the MPG as an executable jar, run the following commands:

```
#!bash
$ cd mpg-api/target
$ java -jar myproxy.jar &
```

### Tomcat webapp

To run MPG in Apache Tomcat, run the following commands:

```
#!bash
$ mvn package
$ cp mpg-api/target/myproxy.war $CATALINA_HOME/webapps/
$ $CATALINA_HOME/bin/shutdown.sh
$ $CATALINA_HOME/bin/shutdown.sh
```

## Configuring

The MPG uses traditional Maven properties for its configuration. The easiest way to do this is adding them as a new profile in your `$HOME/.m2/settings.xml` file. A sample file is available in the root directory of this project. If you are running the service from source, but using the Docker container to host test MyProxy servers, you can use the sample in the `docker` folder for a preconfigured setup. For more information on what the individual settings mean, consult the `mpg-api/src/main/resources/service.properties` file.

